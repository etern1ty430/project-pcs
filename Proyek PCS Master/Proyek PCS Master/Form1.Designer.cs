﻿namespace Proyek_PCS_Master
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.kAMARToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kAMARToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.fASILITASToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jENISToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pEGAWAIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kAMARToolStripMenuItem,
            this.pEGAWAIToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1398, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // kAMARToolStripMenuItem
            // 
            this.kAMARToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kAMARToolStripMenuItem1,
            this.fASILITASToolStripMenuItem,
            this.jENISToolStripMenuItem});
            this.kAMARToolStripMenuItem.Name = "kAMARToolStripMenuItem";
            this.kAMARToolStripMenuItem.Size = new System.Drawing.Size(72, 24);
            this.kAMARToolStripMenuItem.Text = "KAMAR";
            // 
            // kAMARToolStripMenuItem1
            // 
            this.kAMARToolStripMenuItem1.Name = "kAMARToolStripMenuItem1";
            this.kAMARToolStripMenuItem1.Size = new System.Drawing.Size(216, 26);
            this.kAMARToolStripMenuItem1.Text = "RUANGAN";
            this.kAMARToolStripMenuItem1.Click += new System.EventHandler(this.kAMARToolStripMenuItem1_Click);
            // 
            // fASILITASToolStripMenuItem
            // 
            this.fASILITASToolStripMenuItem.Name = "fASILITASToolStripMenuItem";
            this.fASILITASToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.fASILITASToolStripMenuItem.Text = "FASILITAS";
            this.fASILITASToolStripMenuItem.Click += new System.EventHandler(this.fASILITASToolStripMenuItem_Click);
            // 
            // jENISToolStripMenuItem
            // 
            this.jENISToolStripMenuItem.Name = "jENISToolStripMenuItem";
            this.jENISToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.jENISToolStripMenuItem.Text = "JENIS";
            this.jENISToolStripMenuItem.Click += new System.EventHandler(this.jENISToolStripMenuItem_Click);
            // 
            // pEGAWAIToolStripMenuItem
            // 
            this.pEGAWAIToolStripMenuItem.Name = "pEGAWAIToolStripMenuItem";
            this.pEGAWAIToolStripMenuItem.Size = new System.Drawing.Size(83, 24);
            this.pEGAWAIToolStripMenuItem.Text = "PEGAWAI";
            this.pEGAWAIToolStripMenuItem.Click += new System.EventHandler(this.pEGAWAIToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1398, 730);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem kAMARToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kAMARToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem fASILITASToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jENISToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pEGAWAIToolStripMenuItem;
    }
}

