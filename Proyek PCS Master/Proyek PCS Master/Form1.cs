﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyek_PCS_Master
{
    public partial class Form1 : Form
    {
        Form2 ruangan = new Form2();
        Form3 fasilitas = new Form3();
        Form4 jenis = new Form4();
        Form5 pegawai = new Form5();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.IsMdiContainer = true;
        }

        private void kAMARToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (fasilitas != null)
            {
                fasilitas.Close();
            }
            if (jenis != null)
            {
                jenis.Close();
            }
            if (pegawai != null)
            {
                pegawai.Close();
            }
            ruangan = new Form2();
            ruangan.MdiParent = this;
            ruangan.Show();
            ruangan.Location = new Point(0, 0);
        }

        private void fASILITASToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ruangan != null)
            {
                ruangan.Close();
            }
            if (jenis != null)
            {
                jenis.Close();
            }
            if (pegawai != null)
            {
                pegawai.Close();
            }
            fasilitas = new Form3();
            fasilitas.MdiParent = this;
            fasilitas.Show();
            fasilitas.Location = new Point(0, 0);
        }

        private void jENISToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ruangan != null)
            {
                ruangan.Close();
            }
            if (fasilitas != null)
            {
                fasilitas.Close();
            }
            if (pegawai != null)
            {
                pegawai.Close();
            }
            jenis = new Form4();
            jenis.MdiParent = this;
            jenis.Show();
            jenis.Location = new Point(0, 0);
        }

        private void pEGAWAIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ruangan != null)
            {
                ruangan.Close();
            }
            if (fasilitas != null)
            {
                fasilitas.Close();
            }
            if (jenis != null)
            {
                jenis.Close();
            }
            pegawai = new Form5();
            pegawai.MdiParent = this;
            pegawai.Show();
            pegawai.Location = new Point(0, 0);
        }
    }
}
